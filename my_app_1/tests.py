from django.test import TestCase

# Create your tests here.
class MyApp1Test(TestCase):
    
    def test_obvious_things(self):
        self.assertEqual(1+1,2)
        self.assertEqual(1-1,0)
        self.assertEqual(1*1,1)
        self.assertEqual(1/1,1)
